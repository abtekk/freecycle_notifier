﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using System.IO;


namespace FreeCycleScraper
{
    partial class Program
    {
        static void SendMail(List<string> email)
        {
            string bodyText = @"Freecycle hit found:

            <br><br><br>" + string.Join("<br>", email.ToArray());
            MailMessage emailconfig = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");

            emailconfig.From = new MailAddress("");
            emailconfig.To.Add("");
            emailconfig.Subject = @"Freecycle Hit!";

            emailconfig.IsBodyHtml = true;
            emailconfig.Body = bodyText;

            SmtpServer.Port = 587;
            SmtpServer.UseDefaultCredentials = false;
            SmtpServer.Credentials = new System.Net.NetworkCredential("", "");
            SmtpServer.EnableSsl = true;
            SmtpServer.Send(emailconfig);

            Console.WriteLine("Email Sent to " + emailconfig.To.ToString());

        }
    }
}
