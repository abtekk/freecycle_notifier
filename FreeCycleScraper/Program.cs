﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronWebScraper;
using HtmlAgilityPack;

namespace FreeCycleScraper
{
    partial class Program
    {
        public static void Main(string[] args)
        {
            if (!File.Exists(@"AlreadySeen.txt")) { //Create the log file if it doesn't exist.
                System.IO.File.WriteAllText("AlreadySeen.txt", "");
            }

            HtmlAgilityPack.HtmlWeb web = new HtmlAgilityPack.HtmlWeb();

            //New array of key words to loop over.
            string[] keywords = new string[] { "computer", "laptop", "hp", "proliant", "ram", "processor", "phone", "table", "camera", "wardrobe", "garden", "patio", "chair", "excercise", "desk" }; ;
            //New list of keyword hits.
            List<string> hits = new List<string>();
            //New array of URLs to scrape.
            string[] urls = new string[] { @"https://groups.freecycle.org/group/BrentwoodUK/posts/offer", @"https://groups.freecycle.org/group/ChelmsfordUK/posts/offer", @"https://groups.freecycle.org/group/DartfordUK/posts/offer", @"https://groups.freecycle.org/group/SouthendUK/posts/offer" };

            foreach (string url in urls)
            {
                HtmlAgilityPack.HtmlDocument doc = web.Load(url);
                var titleNames = doc.DocumentNode.SelectNodes("//tr/td[2]/a").ToList();
                foreach (string key in keywords)
                {
                    foreach (var title in titleNames)
                    {
                        if (title.InnerText.ToUpper().Contains(key.ToUpper()))
                        {
                            hits.Add("Title: " + title.InnerText + "<br>URL: " + title.OuterHtml);
                        }
                    }
                }
            }

            //Load the Already seen hits in to the existing lists.
            string[] alreadySeen = System.IO.File.ReadAllLines(@"AlreadySeen.txt");
            foreach (string existing in alreadySeen)
            {
                hits.Remove(existing);
            }

            if (hits.Count > 0)
            {
                Console.WriteLine(string.Join("<br>", hits.ToArray()));
                System.IO.File.AppendAllLines("AlreadySeen.txt", hits);
                SendMail(hits);
            }
            
        }
    }
    
}
